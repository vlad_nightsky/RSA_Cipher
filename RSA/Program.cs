﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA
{
    class RSA
    {
        public string Ciper(string message)
        {
            for(int i = 0; i < 10; i++)
            {
                long p = GetPrimeNumber(100);
                long q;
                do
                {
                    q = GetPrimeNumber(100);
                } while (p == q);

                Console.WriteLine("q =  " + q + " " + IsPrime(q,10) + "; p = " + p + " " + IsPrime(p,10));
            }

            Console.Read();/*
            long n = p * q;
            long fi = (p - 1) * (q - 1);*/

            return "ok";
        }

        //РешетоЭратосфена
        long GetPrimeNumber(long max_number)
        {
            bool[] is_composite = new bool[max_number + 1];

            for (long i = 4; i <= max_number; i += 2)
                is_composite[i] = true;

            int next_prime = 3;
            int stop_at = (int)Math.Sqrt(max_number);

            while (next_prime <= stop_at)
            {
                for (long i = next_prime * 2; i < max_number; i += next_prime)
                    is_composite[i] = true;

                next_prime += 2;
                while (next_prime <= max_number && is_composite[next_prime])
                    next_prime += 2;
            }

            List<long> primes=new List<long>();
            for (long i = 2; i < max_number; i++)
                if (!is_composite[i])
                    primes.Add(i);

            Random rand_position = new Random();
            return primes[rand_position.Next(primes.Count())];
        }

        //Тест простоты Ферма
        bool IsPrime(long number, int max_test)
        {
            for(int test = 1; test < max_test; test++)
            {
                Random num = new Random();
                int n = num.Next(1,(int)number - 1);
                long why = BinPow(n, number - 1,number);
                if (why != 1)
                    return false;
            }
            return true;
        }

        //Алгоритм Эвклида
        int GCD(int a,int b)
        {
            while(b != 0)
            {
                int remainder = a & b;
                a = b;
                b = remainder;
            }
            return a;
        }

        long BinMul(long a, long b, long m)
        {
            if (b == 1)
                return a;
            else if(b%2 == 0)
            {
                long t = BinMul(a, b / 2, m);
                return 2 * t % m;
            }

            return (BinMul(a, b - 1, m) + a) % m;
        }

        long BinPow (long a, long b,long m)
        {
            if (b == 0)
                return 1;
            if(b % 2 == 0)
            {
                long t = BinPow(a, b / 2, m);
                return BinMul(t, t, m) % m;
            }
            return BinMul(BinPow(a, b - 1, m), a, m) % m;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            RSA rsa = new RSA();
            rsa.Ciper("test");
        }
    }
}
